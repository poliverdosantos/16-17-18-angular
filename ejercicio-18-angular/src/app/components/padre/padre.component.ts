import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    nombre: "Boby",
    raza: "Labrador",
    edad: "5 años"
  };

  Objeto2 = {
    nombre: "Spanqui",
    raza: "Pastor Aleman",
    edad: "3 años"
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
