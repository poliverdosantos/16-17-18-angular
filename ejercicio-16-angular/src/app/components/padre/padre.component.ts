import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padreMensaje1 = ' Hijo uno ';
  padreMensaje2 = ' Hijo dos ';
  padreMensaje3 = ' Hijo tres ';
  padreMensaje4 = ' Hijo cuatro ';
  padreMensaje5 = ' Hijo cinco   ';
  
  
  constructor() { }

  ngOnInit(): void {
  }

}
